from initializer.base_initializer import BaseInitializer
from initializer.initialization_handler import initialization_handler


class TestInitializer(BaseInitializer):
    def init_run1(self):
        pass

    def init_run2(self):
        pass


class TestInitializer2(BaseInitializer):
    def init_run1(self):
        pass

    def init_run2(self):
        pass


initialization_handler.register(TestInitializer)
initialization_handler.register(TestInitializer2)
