from django.test import TestCase
from django.test.utils import captured_stdout

from initializer.base_initializer import BaseInitializer
from initializer.models import InitializationJob


class BaseInitializerImpl(BaseInitializer):
    def init_run1(self):
        pass

    def init_run2(self):
        pass


class BaseInitializerTestCase(TestCase):
    def setUp(self) -> None:
        self.base_initializer = BaseInitializerImpl()

    def test_run(self):
        with captured_stdout() as out:
            self.base_initializer.run()
            lines = out.getvalue().splitlines()
        self.assertIn("Running init_run1 from BaseInitializerImpl... \x1b[6;30;42mSuccess!\x1b[0m", lines)
        self.assertIn("Running init_run2 from BaseInitializerImpl... \x1b[6;30;42mSuccess!\x1b[0m", lines)

    def test_jobs_clearing(self):
        self.base_initializer.run()
        InitializationJob.objects.clear_jobs()
        count = InitializationJob.objects.all().count()
        self.assertEqual(count, 0)

    def test_job_creation(self):
        self.base_initializer.run()
        init_run1_exists = InitializationJob.objects.exists(
            self.base_initializer.init_run1
        )
        self.assertTrue(init_run1_exists)
        init_run2_exists = InitializationJob.objects.exists(
            self.base_initializer.init_run2
        )
        self.assertTrue(init_run2_exists)
        InitializationJob.objects.clear_jobs()

    def test_multiple_execution_of_same_function(self):
        self.base_initializer.run()
        with captured_stdout() as out:
            self.base_initializer.run()
            lines = out.getvalue().splitlines()
        self.assertIn("init_run1 from BaseInitializerImpl has already been executed, skipping!", lines)
        self.assertIn("init_run2 from BaseInitializerImpl has already been executed, skipping!", lines)



