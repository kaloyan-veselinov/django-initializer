from django.core.management import call_command
from django.test import TestCase
from django.test.utils import captured_stdout

from initializer.models import InitializationJob


class ManagementCommandTestCase(TestCase):
    def test_command(self):
        with captured_stdout() as out:
            call_command('init')
            lines = out.getvalue().splitlines()
            self.assertIn("Running init_run1 from TestInitializer... \x1b[6;30;42mSuccess!\x1b[0m", lines)
            self.assertIn("Running init_run2 from TestInitializer... \x1b[6;30;42mSuccess!\x1b[0m", lines)

    def test_reinitialize_all(self):
        with captured_stdout() as _:
            call_command('init')
        call_command('clearinit')
        count = InitializationJob.objects.all().count()
        self.assertEqual(count, 0)

    def test_reinitialize_single_app(self):
        with captured_stdout() as _:
            call_command('init')
        call_command('clearinit', application="testinitializer2")
        count_1 = InitializationJob.objects.filter(module_name="testinitializer.init").count()
        self.assertEqual(count_1, 4)
        count_2 = InitializationJob.objects.filter(module_name="testinitializer2.init").count()
        self.assertEqual(count_2, 0)

    def test_reinitialize_single_class(self):
        with captured_stdout() as _:
            call_command('init')
        call_command('clearinit', application="testinitializer", initializer="TestInitializer2")
        count_1 = InitializationJob.objects.filter(module_name="testinitializer.init").count()
        self.assertEqual(count_1, 2)

    def test_reinitialize_single_method(self):
        with captured_stdout() as _:
            call_command('init')
        call_command('clearinit', application="testinitializer", initializer="TestInitializer2", method="init_run1")
        count_1 = InitializationJob.objects.filter(module_name="testinitializer.init").count()
        self.assertEqual(count_1, 3)
