from django.test import TestCase
from django.test.utils import captured_stdout

from initializer.base_initializer import BaseInitializer
from initializer.initialization_handler import initialization_handler


class BaseInitializerImpl1(BaseInitializer):
    def init_run(self):
        pass


initialization_handler.register(BaseInitializerImpl1, force=True)


class BaseInitializerImpl2(BaseInitializer):
    priority = 2

    def init_run(self):
        pass


initialization_handler.register(BaseInitializerImpl2, force=True)


class InitializationHandlerTestCase(TestCase):
    def test_run_all(self):
        with captured_stdout() as out:
            initialization_handler.run_initialization()
            lines = out.getvalue().splitlines()

            self.assertIn("Running init_run from BaseInitializerImpl1... \x1b[6;30;42mSuccess!\x1b[0m", lines)
            self.assertIn("Running init_run from BaseInitializerImpl2... \x1b[6;30;42mSuccess!\x1b[0m", lines)

    def test_run_in_order(self):
        with captured_stdout() as out:
            initialization_handler.run_initialization()
            lines = out.getvalue().splitlines()

            index_run_1 = lines.index("Running init_run from BaseInitializerImpl1... \x1b[6;30;42mSuccess!\x1b[0m")
            index_run_2 = lines.index("Running init_run from BaseInitializerImpl2... \x1b[6;30;42mSuccess!\x1b[0m")
            self.assertGreater(index_run_1, index_run_2)
