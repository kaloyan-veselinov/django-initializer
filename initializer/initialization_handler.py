from itertools import chain

from django.conf import settings
from django.contrib.admin.sites import AlreadyRegistered
from django.utils.module_loading import autodiscover_modules, import_string

from initializer.base_initializer import BaseInitializer
from initializer.models import InitializationJob


class InitializationHandler(object):
    def __init__(self):
        self._registry = {}

    def register(self, initializer_class, force: bool = False):
        assert issubclass(initializer_class, BaseInitializer)

        module_name = initializer_class.__module__
        if not force:
            application_name = module_name.rsplit('.', 1)[0]
            assert application_name in settings.INSTALLED_APPS
        if module_name not in self._registry:
            self._registry[module_name] = []

        if initializer_class in self._registry[module_name]:
            raise AlreadyRegistered

        self._registry[module_name].append(initializer_class)

    def run_initialization(self):
        all_classes = chain.from_iterable(self._registry.values())
        sorted_classes = sorted(all_classes, key=lambda c: c.priority, reverse=True)
        for cls in sorted_classes:
            initializer_instance = cls()
            initializer_instance.run()

    def clear_all_jobs(self):
        InitializationJob.objects.clear_jobs()

    def clear_jobs_for_application(self, application_name: str):
        module_name = f"{application_name}.init"
        assert module_name in self._registry
        InitializationJob.objects.clear_jobs(module_name=module_name)

    def clear_jobs_for_class(self, application_name: str, class_name: str):
        module_name = f"{application_name}.init"
        assert module_name in self._registry
        cls = import_string(f"{module_name}.{class_name}")

        for method in cls().init_methods:
            InitializationJob.objects.clear_jobs(method=method)

    def clear_jobs_for_method(self, application_name: str, class_name: str, method_name: str):
        module_name = f"{application_name}.init"
        assert module_name in self._registry
        cls = import_string(f"{module_name}.{class_name}")
        assert hasattr(cls, method_name)
        method = getattr(cls(), method_name)
        InitializationJob.objects.clear_jobs(method=method)


initialization_handler = InitializationHandler()
autodiscover_modules("init", register_to=initialization_handler)
