from django.db import models


class InitializationJobManager(models.Manager):
    def create_job(self, method):
        return self.create(
            module_name=method.__module__,
            method_name=method.__qualname__
        )

    def get_job(self, method):
        return self.get(
            module_name=method.__module__,
            method_name=method.__qualname__
        )

    def exists(self, method):
        try:
            self.get_job(method)
            return True
        except models.ObjectDoesNotExist:
            return False

    def clear_jobs(self, module_name: str = None, method=None):
        if module_name:
            queryset = self.filter(module_name=module_name)
        elif method:
            queryset = self.filter(
                module_name=method.__module__,
                method_name=method.__qualname__
            )
        else:
            queryset = self.all()
        queryset.delete()


class InitializationJob(models.Model):
    module_name = models.CharField(
        "Name of the module",
        max_length=255
    )
    method_name = models.CharField(
        "Fully qualified name of the executed method, i.e. class.method",
        max_length=80
    )
    objects = InitializationJobManager()

    class Meta:
        unique_together = ['module_name', 'method_name']
